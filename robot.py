#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2019/11/11 11:11 AM
# @Author  : Angelo
# @Site    :
# @File    : auto_archive_ios.py
# @Software: PyCharm


import os
import requests
import webbrowser
import subprocess
import time
import smtplib

project_name = 'blackcard'    # 项目名称
archive_workspace_path = '/Users/caochao/Desktop/workplace/release/mofangapp-ios2.0'    # 项目路径
export_directory = 'archive'    # 输出的文件夹
ipa_path = '/Users/caochao/Desktop/ipa/out'

class AutoArchive(object):

    def __init__(self):
        pass
       
    def clean(self):
        print("\n\n===========开始clean操作===========")
        # 删除路径ipa包
        subprocess.call(['rm', '-rf', '%s' % (ipa_path)])
        start = time.time()
        clean_command = 'xcodebuild clean -workspace %s/%s.xcworkspace -scheme %s -configuration %s' % (
            archive_workspace_path, project_name, project_name,description)
        clean_command_run = subprocess.Popen(clean_command, shell=True)
        clean_command_run.wait()
        end = time.time()
        # Code码
        clean_result_code = clean_command_run.returncode
        if clean_result_code != 0:
            print("=======clean失败,用时:%.2f秒=======" % (end - start))
        else:
            print("=======clean成功,用时:%.2f秒=======" % (end - start))
            self.archive()

    def archive(self):
        print("\n\n===========开始archive操作===========")
        
        # 删除之前的文件
        subprocess.call(['rm', '-rf', '%s/%s' % (archive_workspace_path, export_directory)])
        #time.sleep(1)
        # 创建文件夹存放打包文件
        subprocess.call(['mkdir', '-p', '%s/%s' % (archive_workspace_path, export_directory)])
        #time.sleep(1)

        start = time.time()
        archive_command = 'xcodebuild archive -workspace %s/%s.xcworkspace -scheme %s -configuration %s -archivePath %s/%s/%s' % (
            archive_workspace_path, project_name, project_name,description,archive_workspace_path, export_directory, export_directory)
        archive_command_run = subprocess.Popen(archive_command, shell=True)
        archive_command_run.wait()
        end = time.time()
        # Code码
        archive_result_code = archive_command_run.returncode
        if archive_result_code != 0:
            print("=======archive失败,用时:%.2f秒=======" % (end - start))
        else:
            print("=======archive成功,用时:%.2f秒=======" % (end - start))
            # 导出IPA
            self.export()

    def export(self):
        print("\n\n===========开始export操作===========")
        print("\n\n==========请你耐心等待一会~===========")
        start = time.time()
        # /Users/caochao/Desktop/ipa/out //ipa输出路径
        # /Users/caochao/Desktop/ExportOptions.plist  //配置文件
        export_command = 'xcodebuild -exportArchive -archivePath /Users/caochao/Desktop/workplace/release/mofangapp-ios2.0/archive/archive.xcarchive -exportPath /Users/caochao/Desktop/ipa/out -exportOptionsPlist /Users/caochao/Desktop/ExportOptions.plist'
        export_command_run = subprocess.Popen(export_command, shell=True)
        export_command_run.wait()
        end = time.time()
        end1 = time.time()
        # Code码
        export_result_code = export_command_run.returncode
        if export_result_code != 0:
            print("=======导出IPA失败,用时:%.2f秒=======" % (end - start))
        else:
            print("=======导出IPA成功,用时:%.2f秒=======" % (end - start))

        if not os.path.exists(ipa_path):
            print("目录不存在!!")
            os._exit(1)
        filenames = os.listdir(ipa_path)
        for filename in filenames:
            if filename == "blackcard.ipa":
                if description == "Debug":
                    os.rename("/Users/caochao/Desktop/ipa/out/blackcard.ipa","/Users/caochao/Desktop/ipa/out/blackcard_dev.ipa")

            # 删除archive.xcarchive文件
            subprocess.call(['rm', '-rf', '%s/%s' % (archive_workspace_path, export_directory)])

if __name__ == '__main__':

    description = input("请选择打包环境1.Release或2.Debug :")
    if description == "1":
        description = "Release"
    else:
        description = "Debug"

    archive = AutoArchive()
    archive.clean()
